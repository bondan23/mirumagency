<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'category_id',
        'user_id',
        'title',
        'slug',
        'short_description',
        'content',
        'image',
        'thumbnail',
        'status'
    ];
    
    protected $casts = [
        'status' => 'boolean',
    ];
    
    public function getCreatedAtAttribute($date) {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('M d, Y H:i'); 
    }
    
    public function categories(){
        return $this->belongsTo('App\Category','category_id'); //merubah foreign key
    }
    
    public function users(){
        return $this->belongsTo('App\User','user_id'); //merubah foreign key
    }
    
    public function comments(){
        return $this->hasMany('App\Comment')->orderBy('created_at','desc');
    }
}
