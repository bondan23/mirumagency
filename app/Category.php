<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['categories','user_id'];
    
    public function articles(){
        return $this->hasMany('App\Article');
    }
    
    public function users(){
        return $this->belongsTo('App\User','user_id'); //merubah foreign key
    }
    
    public function articlesCount()
    {
        return $this->hasMany('App\Article')->selectRaw('category_id, count(*) as hitung')->where('status',true)->groupBy('category_id');
    }
    
    // and accessor for fetching it easier
    public function getArticlesCountAttribute()
    {
        return $this->articlesCount()->first() ? $this->articlesCount()->first()->hitung : 0;
    }
}
