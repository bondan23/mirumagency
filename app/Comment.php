<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'article_id', 'user_id', 'comment',
    ];
    
    public function getCreatedAtAttribute($date) {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('M d, Y H:i'); 
    }
    
    public function articles(){
        return $this->belongsTo('App\Articles','article_id'); //merubah foreign key
    }
    
    public function users(){
        return $this->belongsTo('App\User','user_id'); //merubah foreign key
    }
}
