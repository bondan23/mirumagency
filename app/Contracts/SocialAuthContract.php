<?php namespace App\Contracts;

interface SocialAuthContract {
    public function redirectToProvider();
    public function handleProviderCallback();
    public function findOrCreateUser($user);
}