<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct(){
        //hanya boleh di masuki admin
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }

    /*Change Password Here*/
    public function changePasswordView()
    {
        return view('admin.auth.changepass');
    }
    
    public function changePasswordStore(Request $request,$id)
    {
        \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {

            return \Hash::check($value, \Auth::user()->password);

        });
        
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'old_password'=> 'required|old_password:'
        ],[
            'old_password'=> 'The old password did not match.'
        ]);
        
        $user = \App\User::find($id);
        $user->password = $request->input('password_confirmation');
        $save = $user->save();
        
        if($save)
        {
            $request->session()->flash('flash_notification.message', 'Sukses Merubah Passowrd !!');
            return redirect('/admin');
        }
    }
}
