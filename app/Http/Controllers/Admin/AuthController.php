<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    //protected $loginView = 'admin.auth.login';

    protected $guard = 'admin';

    protected $redirectTo = '/admin';
    
    
    public function showLoginForm()
    {
        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('admin.auth.login');
    }
    
    protected function guard()
    {
        return Auth::guard('admin');
    }
}