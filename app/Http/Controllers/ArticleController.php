<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Category;
use Validator;
use Auth;
use Image;

class ArticleController extends Controller
{
    
    public function __construct(){
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $article = Article::with('categories','users')
                ->where('user_id',Auth::user()->id)
                ->orderBy('created_at', 'desc')
                ->get();
        
        $this->data['article'] = $article;
        return view('article.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('categories','id');
        $category->prepend("Silahkan Pilih Category",0);
        $this->data['category'] = $category;
        $this->data['article'] = new Article;
        return view('article.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !'
        ];
        
        $validator = Validator::make($request->all(),[
           'title'=>'required',
           'content'=>'required',
           'image'=>'required',
           'category_id'=>'not_in:0'
        ],$messages);
        
        
        if($validator->fails()){
            return redirect()
                ->route('article.create')
                ->withErrors($validator,'form')
                ->withInput();
        }
        else{
        
            $post = array_except($request->input(),['_token']);
            $post['slug'] = str_slug($post['title']);


            //upload file
            $photo = $request->file('image');
            $filename = str_random(6) . "." . $photo->getClientOriginalExtension(); 
            $path = public_path() . '/uploaded_file';
            $photo->move($path, $filename);

            $post['short_description'] = strip_tags(str_limit($post['content'],150));
            $post['user_id'] = Auth::user()->id;
            $post['image'] = $filename;
            
            /*Making Thumbnail*/
            $path_thumbnail = public_path() . '/uploaded_file/'.$filename;
            $thumbnail = Image::make($path_thumbnail)->resize(320, 240)->save("uploaded_file/thumb_".$filename);
            $post['thumbnail'] = "thumb_".$filename;
            $post['status'] = false;

            $create = Article::create($post);
            
            if($create)
            {
                return redirect()->route('article.index');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::with('users','categories')->where('id',$id)->first();
        
        $category = Category::all();
        
        if($id == null){
            return view('errors.404');
        }
        else{
            if($article == null)
            {
                return view('errors.404');
            }
            else{
                $this->data['category'] = $category;
                $this->data['article'] = $article;
                return view('article_detail',$this->data);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->id;
        
        $article = Article::with('categories','users')
            ->where('id',$id)
            ->orderBy('created_at', 'desc')
            ->first();
        
        $this->data['article'] = $article;
        
        $category = Category::pluck('categories','id');
        $this->data['category'] = $category;
        
        return view('article.edit',$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !'
        ];
        
        $validator = Validator::make($request->all(),[
           'title'=>'required',
           'content'=>'required',
        ],$messages);
        
        
        if($validator->fails()){
            return redirect()
                ->route('article.edit',$id)
                ->withErrors($validator,'form')
                ->withInput();
        }
        else{
            //upload file
            $post = array_except($request->input(),['_method','_token']);
            
            //search data by PK
            $article = Article::find($id);
            
            if($request->file('image') != null){
                //proses upload foto
                $photo = $request->file('image');
                $filename = str_random(6) . "." . $photo->getClientOriginalExtension(); 
                $path = public_path() . '/uploaded_file';
                $photo->move($path, $filename);
                
                //binding nama file baru
                $article->image = $filename;
                /*Making Thumbnail*/
                $path_thumbnail = public_path() . '/uploaded_file/'.$filename;
                $thumbnail = Image::make($path_thumbnail)->resize(320, 240)->save("uploaded_file/thumb_".$filename);
                $article->thumbnail = "thumb_".$filename;
                
                //hapus foto lama setelah upload foto baru,agar mengurangi space
                unlink($path."/".$post['oldimage']);
                unlink($path."/"."thumb_".$post['oldimage']);
            }
            
            $article->category_id = $post['category_id'];
            $article->title = $post['title'];
            $article->slug = str_slug($post['title']);
            $article->short_description = strip_tags(str_limit($post['content'],150));
            $article->content = $post['content'];
            $save = $article->save();
            
            if($save){
                return redirect()->route('article.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $findImage = Article::find($id);
        $image = $findImage->image;
        $path = public_path() . '/uploaded_file';
        unlink($path."/".$image);
        $findImage->delete();
        
    }
}
