<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        
        //admin yg sudah login dilarang masuk ke area user
        $this->middleware('guest:admin');
    }
    
    public function redirectPath()
    {
        $redirect = \Input::get('redirect');
        
        if(isset($redirect))
        {
            $slug = \App\Article::find($redirect);
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/view/'.$slug->slug;
        }
        else{
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
        }
    }
}
