<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::where('user_id',\Auth::user()->id)->get();
        $this->data['category'] = $category;
        
        return view('category.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;
        $this->data['category'] = $category;
        
        return view('category.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !'
        ];
        
        $this->validate($request,['categories'=>'required|min:3|unique:categories'],$messages);
        
        $post = array_except($request->input(),['_token']);
        $post['user_id'] = \Auth::user()->id;
        $create = Category::create($post);
        
        if($create)
        {
            return redirect()->route('category.index');
        }
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $this->data['category'] = $category;
        return view('category.edit',$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !'
        ];
        
        $this->validate($request,['categories'=>'required|min:3|unique:categories'],$messages);
        
        $post = array_except($request->input(),['_method','_token']);
        $category = Category::find($id);
        $category->categories = $post['categories'];
        $save = $category->save();
        
        if($save){
            return redirect()->route('category.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }
}
