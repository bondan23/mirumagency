<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Article;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        if ($request->ajax()){
            $comments = Comment::with('users')->where('article_id',$id)->get();
            return view('comment.index',compact('comments'));
        }
        else{
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$article_id)
    {
        $this->validate($request,['comment'=>'required']);
        
        $post = array_except($request->input(),['_token']);
        $post['article_id'] = $article_id;
        $post['user_id'] = (Auth::guard('admin')->guest() == false)?Auth::guard('admin')->user()->id:Auth::user()->id;
        $create = Comment::create($post);
        $article = Article::find($article_id);
        
        if($create)
        {
            return redirect()->route('blog.view',$article->slug);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($article_id,$id)
    {
        $comment = Comment::find($id);
        $comment->delete();
    }
}
