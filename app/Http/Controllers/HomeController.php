<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use Faker\Factory as Faker;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function landingPage(Request $request){
        $category = Category::all();
        
        if($request->get('q'))
        {
            $article = Article::where('title','like','%'.$request->get('q').'%')->paginate(2);
            $this->data['q'] = $request->get('q');
        }
        else{
            $article = Article::with('categories','users')
                ->where('status',true)
                ->orderBy('created_at', 'desc')
                ->paginate(2);
        }
        
        $this->data['category'] = $category;
        $this->data['article'] = $article;
        return view('landing',$this->data);
    }
    
    public function detail($slug = null)
    {
        $category = Category::all();
        
        $article = Article::with('users','categories','comments')
            ->where('slug',$slug)
            ->where('status',true)
            ->first();
        
        if($slug == null){
            return view('errors.404');
        }
        else{
            if($article == null)
            {
                return view('errors.404');
            }
            else{
                $this->data['category'] = $category;
                $this->data['article'] = $article;
                return view('article_detail',$this->data);
            }
        }
    }
}
