<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use Auth;
use App\User;
class SocialController extends Controller
{
    public function redirect($provider){
        return app($provider)->redirectToProvider();
    }
    
    public function callback($provider){
        return app($provider)->handleProviderCallback();
    }
}
