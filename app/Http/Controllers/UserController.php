<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
        //admin tidak boleh masuk ke dashboard user
        $this->middleware('guest:admin');
        //$this->middleware('auth:admin',['except'=>['landingPage','detail','index']]);
    }
    
    
    public function index(Request $request)   
    {
        //Dashboard Khusus User..
        return view('home');
    }
    
    
    /*Change Password Here*/
    public function changePasswordView()
    {
        return view('auth.changepass');
    }
    
    public function changePasswordStore(Request $request,$id)
    {
        \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {

            return \Hash::check($value, \Auth::user()->password);

        });
        
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'old_password'=> 'required|old_password'
        ],[
            'old_password'=> 'The old password did not match.'
        ]);
        
        $user = \App\User::find($id);
        $user->password = $request->input('password_confirmation');
        $save = $user->save();
        
        if($save)
        {
            $request->session()->flash('flash_notification.message', 'Sukses Merubah Passowrd !!');
            return redirect('/home');
        }
    }
}
