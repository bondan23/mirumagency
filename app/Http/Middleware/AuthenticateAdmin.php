<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    //middleware ini lah yang membuat user biasa harus login lagi ketika masuk ke halaman admin.
    
    
    public function handle($request, Closure $next,$guard)
    {
        if (Auth::guard($guard)->guest() || !Auth::guard($guard)->user()->is_admin) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect('/admin/login');
            }
        }
        return $next($request);
    }
}
