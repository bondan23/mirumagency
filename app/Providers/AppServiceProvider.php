<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        //$this->app->bind('App\Contracts\SocialAuthContract','App\Socialauth\FacebookAuth');
        $this->app->bind('facebook',function($app){
            return new \App\Socialauth\FacebookAuth($app->make('Contracts\SocialAuthContract'));
        });
        
        $this->app->bind('twitter',function($app){
            return new \App\Socialauth\TwitterAuth($app->make('Contracts\SocialAuthContract'));
        });
    }
}
