<?php

namespace App;

use App\Contracts\SocialAuthContract as SocialAuth;
use Socialite;
use Auth;
class SocialMediaAuth {
    protected $provider;
    
    public function __construct(SocialAuth $provider)
    {
        $this->provider = $provider;
    }
    
    public function redirect(){
        return $this->provider->redirectToProvider();
    }
    
    public function callback(){
        return $this->provider->handleProviderCallback();
    }
}