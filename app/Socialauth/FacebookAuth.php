<?php

namespace App\Socialauth;
use App\Contracts\SocialAuthContract as SocialAuth;
use Socialite;
use Auth;
use App\User;
use Faker\Factory as Faker;
class FacebookAuth implements SocialAuth {
    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     * Code ini di gunakan untuk mengarahkan ke oAuth facebook
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     * Code ini di gunakan setelah menadapat response true dari facebook,akan di arahkan ke callback ini
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (\Exception $e) {
            //Jika Cancel saaat login facebook
            return redirect('/');
        }

        $authUser = $this->findOrCreateUser($user);
        
        if($authUser->is_admin == true)
        {
            //Jika login dengan facebook sebagai user yang sudah di jadikan admin
            Auth::guard('admin')->login($authUser,true);
            return redirect('/admin');
        }
        else{
            //Jika login dengan facebook sebagai user
            Auth::login($authUser, true);
            return redirect('/home');
        }
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    public function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('email', $facebookUser->email)->first();

        if ($authUser){
            return $authUser;
        }
        $faker = Faker::create('id_ID');
        
        return User::create([
            'fullname' => $facebookUser->name,
            'email' => ($facebookUser->email != "")?$facebookUser->email:$faker->email,
            'password' => "defaultpassword",
            'is_admin' => false,
            'social_login' => true,
        ]);
    }
}