<?php

namespace App\Socialauth;
use App\Contracts\SocialAuthContract as SocialAuth;
use Socialite;
use Auth;
use App\User;
use Faker\Factory as Faker;
class TwitterAuth implements SocialAuth {
    /**
     * Redirect the user to the twitter authentication page.
     *
     * @return Response
     * Code ini di gunakan untuk mengarahkan ke oAuth twitter
     */
    public function redirectToProvider()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from twitter.
     *
     * @return Response
     * Code ini di gunakan setelah menadapat response true dari twitter,akan di arahkan ke callback ini
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('twitter')->user();
        } catch (\Exception $e) {
            //Jika Cancel saaat login twitter
            return redirect('/');
        }

        $authUser = $this->findOrCreateUser($user);
        
        if($authUser->is_admin == true)
        {
            //Jika login dengan twitter sebagai user yang sudah di jadikan admin
            Auth::guard('admin')->login($authUser,true);
            return redirect('/admin');
        }
        else{
            //Jika login dengan twitter sebagai user
            Auth::login($authUser, true);
            return redirect('/home');
        }
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $twitterUser
     * @return User
     */
    public function findOrCreateUser($twitterUser)
    {
        $authUser = User::where('email', $twitterUser->email)->first();

        if ($authUser){
            return $authUser;
        }
        $faker = Faker::create('id_ID');
        
        return User::create([
            'fullname' => $twitterUser->name,
            'email' => ($twitterUser->email != "")?$twitterUser->email:$faker->email,
            'password' => "defaultpassword",
            'is_admin' => false,
            'social_login' => true,
        ]);
    }
}