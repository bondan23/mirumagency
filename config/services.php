<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '580136955514240',
        'client_secret' => '12b3b187046e2227441704cb2c85dad4',
        'redirect' => 'http://mirumagency.app/socialauth/facebook/callback',
    ],
    'twitter' => [
        'client_id' => 'xuIYpOte8jaQebBEBKU9yGnrm',
        'client_secret' => 'UW7FXluiNW3gQM8QQg2TnP1TbwZZwNnamN1WoOZYS0oOEFaSRL',
        'redirect' => 'http://mirumagency.app/socialauth/twitter/callback',
    ],

];
