<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('title',100);
            $table->string('slug',100);
            $table->string('short_description',255);
            $table->text('content');
            $table->string('image',100);
            $table->string('thumbnail',100)->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            
            $table->foreign('category_id')
              ->references('id')->on('categories')
              ->onDelete('cascade');
            
            $table->foreign('user_id')
              ->references('id')->on('users')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
