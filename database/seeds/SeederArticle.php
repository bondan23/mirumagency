<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class SeederArticle extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Hapus File
        \File::cleanDirectory(public_path('uploaded_file'));
        
        $faker = Faker::create('id_ID');
        
        DB::table('articles')->delete();
        
        $article = array(
            array(
                'category_id' => '1',
                'user_id' => '1',
                'title' => 'How to promote your blog in the coolest way',
                'slug' => str_slug('How to promote your blog in the coolest way'),
                'short_description' => str_limit($faker->text(200),100),
                'content' => $faker->text(500),
                'image' => $faker->image(public_path('uploaded_file'),760,600,'cats',false),
                'thumbnail' => null,
                'status' => true,
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
            array(
                'category_id' => '2',
                'user_id' => '2',
                'title' => 'How to use AngularJS in the easiest way',
                'slug' => str_slug('How to use AngularJS in the easiest way'),
                'short_description' => str_limit($faker->text(200),100),
                'content' => $faker->text(500),
                'image' => $faker->image(public_path('uploaded_file'),760,600,'cats',false),
                'thumbnail' => null,
                'status' => true,
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
        );
        
        DB::table('articles')->insert($article);
        
        $this->command->info('Sukses Memasukan 2 Article Dummy');
    }
}
