<?php

use Illuminate\Database\Seeder;
class SeederCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        
        $user = array(
            array(
                'user_id' => '1',
                'categories' => 'SEO',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
            array(
                'user_id' => '2',
                'categories' => 'Front-End',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
            array(
                'user_id' => '2',
                'categories' => 'Back-End',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
        );
        
        DB::table('categories')->insert($user);
        
        $this->command->info('Sukses Memasukan 3 Category Dummy');
    }
}
