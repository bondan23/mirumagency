<?php

use Illuminate\Database\Seeder;

class SeederUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        
        $user = array(
            array(
                'fullname' => 'Administrator',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin123'),
                'remember_token' => str_random(10),
                'is_admin' => true,
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
            array(
                'fullname' => 'User Testing',
                'email' => 'bondan@bondan.com',
                'password' => bcrypt('admin123'),
                'remember_token' => str_random(10),
                'is_admin' => false,
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
        );
        
        DB::table('users')->insert($user);
        
        $this->command->info('1. admin@admin.com, 2. bondan@bondan.com | both default password "admin123');
    }
}
