function hapus_modal(x)
{
    var id = $(x).attr('data-id');
    var route = $(x).attr('data-route');
    var source = $(x).attr('data-hapus');
    var redirect = $(x).attr('data-redirect');
    var source_class = $('.hapus_'+source);

    source_class.attr('data-route',route);
    source_class.attr('data-rest',source);
    source_class.attr('data-redirect',redirect);
    console.log(route);
}

function hapus(x)
{
    var route = $(x).attr('data-route');
    var rest = $(x).attr('data-rest');
    var redirect = $(x).attr('data-redirect');
    $.ajax({
        url:link+route,
        method:"DELETE",
        success:function(data)
        {
            window.location.href=redirect;
            $('#confirmModal').modal('hide');
        }
    })
}

function get_comment(id)
{
    $.get(link+'article/'+id+'/comment',function(data){
        $('.content_comments').html(data);
    });
}

function delete_comment(article_id,comment_id){
    var x = confirm('Yakin Ingin Hapus Komentar?');
    var redirect = $('.content_comments').attr('data-redirect');
    if(x == true){
        $.ajax({
            url:link+'article/'+article_id+'/comment/'+comment_id,
            method:"DELETE",
            success:function(data)
            {
                window.location.href=redirect;
                $('#commentModal').modal('hide');
            }
        })
    }
}

$(document).ready(function(){
    $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    
    $('.check_all').click(function () {
     $('input[type="checkbox"]').prop('checked', this.checked);    
 });
});