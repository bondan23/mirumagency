# This Program is Made by Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

**This Application is Built in Laravel Homestead Environment**

**Requirement:**

* PHP >= 5.6
* This APP_URL on .env must be mirumagency.app if you want use Facebook oAuth For login

**How to use:**

1. Simply clone this repository
2. Install Composer from your Terminal/Command Prompt (composer install)
3. Create .env from .env-example and config your database,makesure you use homestead and set the APP_URL base on requirement,otherwise you cannot use facebook login.
4. Generate the new key (php artisan key:generate)
5. Simply migrate your database and seed the dummy (php artisan migrate --seed)
6. Enjoy the application !

Any question ? contact me at bondan.ekoprasetyo@gmail.com

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).