{!! Form::model($article,array('route'=>$route,'method'=>$method,'enctype'=>'multipart/form-data'))!!}
   <div class="form-group">
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('category_id', 'Category') !!}
                {!! Form::select('category_id', $category,$article->category_id,['class' => 'form-control']) !!}
                @if (count($errors->form) > 0)
                    <b class="text-red">{{ $errors->form->first('category_id') }}</b>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('title', 'Title') !!}
                {!! Form::text('title', old('title',$article->title), ['class' => 'form-control']) !!}
                @if (count($errors->form) > 0)
                    <b class="text-red">{{ $errors->form->first('title') }}</b>
                @endif
            </div>
        </div>
    </div>
   <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('content', 'Content') !!}
                {!! Form::textarea('content', old('content',$article->content), ['class' => 'form-control','id'=>'editor']) !!}
                @if (count($errors->form) > 0)
                    <b class="text-red">{{ $errors->form->first('content') }}</b>
                @endif
            </div>
        </div>
    </div>
    @if($method == 'PUT')
   <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('Image', 'Current Image') !!}
                {!! Form::hidden('oldimage',$article->image) !!}
                <img class="img-responsive" src="{{ asset('uploaded_file').'/'.$article->image }}" alt="">
            </div>
        </div>
    </div>
    @endif
   <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('image', 'Upload Image') !!}
                {!! Form::file('image', null, ['class' => 'form-control']) !!}
                @if (count($errors->form) > 0)
                    <b class="text-red">{{ $errors->form->first('image') }}</b>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <!-- /.col -->
        <div class="col-md-4 col-md-offset-8">
            {!! Form::submit($method == 'PUT' ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block btn-flat']) !!}
        </div>
        <!-- /.col -->
    </div>
{!! Form::close() !!}