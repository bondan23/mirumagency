@extends('layouts.cms')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Your Article</div>

                    <div class="panel-body">
                       <a href="{{ route('admin.article.create') }}" class="btn btn-primary" style="margin-bottom:10px;">
                           <i class="fa fa-plus"></i> Add New Article
                       </a>
                       @if($article->count() == 0)
                           <h2 class="text-center" style="margin-top:0px;">Belum Ada Artikel Tersedia</h2>
                       @else
                        <form method="POST" action="{{ route('admin.article.approve')}}" role="form">
                            {{ csrf_field() }}
                            <table class="table table-condensed table-bordered">
                                <tr class="text-center">
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox" class="check_all">
                                            </label>
                                        </div>
                                    </td>
                                    <td>No</td>
                                    <td>Thumbnail</td>
                                    <td>Title</td>
                                    <td>Short Description</td>
                                    <td>Posted by</td>
                                    <td>Status</td>
                                    <td>Category</td>
                                    <td>Comment</td>
                                    <td>
                                        <i class="fa fa-gear"></i>
                                    </td>
                                </tr>
                                <?php 
                                    $no=1;
                                ?>
                                @foreach($article as $v)
                                <tr class="{{ ($v->status == true)?'success':'warning' }}">
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="selected[]" value="{{ $v->id }}">
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        {{ $no++ }}
                                    </td>
                                    <?php 
                                        $image = ($v->thumbnail == null)?$v->image:$v->thumbnail;
                                    ?>
                                    <td>
                                        <img class="img-responsive" src="{{ asset('uploaded_file').'/'.$image }}" alt="">
                                    </td>
                                    <td>
                                        {{ $v->title }}
                                    </td>
                                    <td>
                                        {{ $v->short_description }}
                                    </td>
                                    <td>
                                        {{ $v->users->fullname }}
                                    </td>
                                    <td>
                                        @if($v->status == false)
                                            Unpublished
                                        @else
                                            Published
                                        @endif
                                    </td>
                                    <td>
                                        {{ $v->categories->categories }}
                                    </td>
                                    <td class="text-center">
                                        <a href="#" data-toggle="modal" data-target="#commentModal" onclick="get_comment('{{ $v->id }}')">
                                            <span class="badge">
                                                {{ $v->comments->count() }}
                                            </span>
                                        </a>
                                    </td>
                                    <td class="text-center" style="width:15%">
                                        <a href="{{ route('admin.article.edit',$v->id) }}" class="btn btn-primary btn">
                                            <i class="fa fa-pencil"></i> Edit
                                        </a>
                                        <a href="javascript:void(0)" onclick="hapus_modal(this)"  data-id="{{ $v->id }}"  class="btn btn-danger" data-hapus="article" data-redirect="view" data-route="admin/article/delete/{{$v->id}}" data-toggle="modal" data-target="#confirmModal">
                                            <i class="fa fa-trash"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            {!! Form::submit('Publish',['name'=>'approve','class'=>'btn btn-success'])!!}
                            {!! Form::submit('Unpublish',['name'=>'unapprove','class'=>'btn btn-warning'])!!}
                            {!! Form::submit('Delete All',['name'=>'deleteall','class'=>'btn btn-danger','onclick'=>'return confirm("Yakin ingin hapus article ??")'])!!}
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('modal')
    @include('modal_confirm',['modal_title'=>'Article'])
    @include('modal_comment',['redirect'=>'view'])
@endsection