{!! Form::model($category,array('route'=>$route,'method'=>$method,'enctype'=>'multipart/form-data'))!!}
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('categories', 'Category') !!}
                {!! Form::text('categories', old('categories',$category->categories), ['class' => 'form-control']) !!}
                @if ($errors->has('categories'))
                    <span class="help-block">
                        <strong>{{ $errors->first('categories') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <!-- /.col -->
        <div class="col-md-4 col-md-offset-8">
            {!! Form::submit($method == 'PUT' ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block btn-flat']) !!}
        </div>
        <!-- /.col -->
    </div>
{!! Form::close() !!}