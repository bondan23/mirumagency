@extends('layouts.cms')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Your Category</div>

                    <div class="panel-body">
                       <a href="{{ route('admin.category.create') }}" class="btn btn-primary" style="margin-bottom:10px;">
                           <i class="fa fa-plus"></i> Add New Category
                       </a>
                        @if($category->count() == 0)
                           <h2 class="text-center" style="margin-top:0px;">Belum Ada Artikel Tersedia</h2>
                        @else
                        <table class="table table-condensed table-striped table-bordered">
                            <tr class="text-center">
                                <td>No</td>
                                <td>Category</td>
                                <td>
                                    <i class="fa fa-gear"></i>
                                </td>
                            </tr>
                            <?php $no=1; ?>
                            @foreach($category as $v)
                            <tr>
                                <td class="text-center" style="width:10%">
                                    {{ $no++ }}
                                </td>
                                <td class="text-center">
                                    {{ $v->categories }}
                                </td>
                                <td class="text-center" style="width:25%">
                                    <a href="{{ route('admin.category.edit',$v->id) }}" class="btn btn-primary btn-xs">
                                        <i class="fa fa-pencil"></i> Edit
                                    </a>
                                    <a href="javascript:void(0)" onclick="hapus_modal(this)"  data-id="{{$v->id}}"  class="btn btn-danger btn-xs" data-hapus="category" data-redirect="view" data-route="admin/category/delete/{{ $v->id }}" data-toggle="modal" data-target="#confirmModal">
                                        <i class="fa fa-trash"></i> Delete
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('modal')
    @include('modal_confirm',['modal_title'=>'Category'])
@endsection