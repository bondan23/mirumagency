{!! Form::model($user,array('route'=>$route,'method'=>$method,'enctype'=>'multipart/form-data'))!!}
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('fullname', 'Fullname') !!}
                {!! Form::text('fullname', old('categories',$user->fullname), ['class' => 'form-control']) !!}
                @if ($errors->has('fullname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fullname') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', old('categories',$user->email), ['class' => 'form-control']) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('is_admin', 'Role') !!}
                {!! Form::select('is_admin', ['User','Admin'],null,['class' => 'form-control']) !!}
                @if (count($errors->form) > 0)
                    <b class="text-red">{{ $errors->form->first('is_admin') }}</b>
                @endif
            </div>
        </div>
    </div>
    @if($method !== 'PUT')
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password',['class' => 'form-control']) !!}
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('password_confirmation', 'Password Confirmation') !!}
                {!! Form::password('password_confirmation',['class' => 'form-control']) !!}
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <!-- /.col -->
        <div class="col-md-4 col-md-offset-8">
            {!! Form::submit($method == 'PUT' ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-block btn-flat']) !!}
        </div>
        <!-- /.col -->
    </div>
{!! Form::close() !!}