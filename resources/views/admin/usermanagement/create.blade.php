@extends('layouts.cms')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Create User</div>

                    <div class="panel-body">
                       @include('admin.usermanagement._form',['route'=>'admin.usermanagement.store','user'=>$user,'method'=>'POST'])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection