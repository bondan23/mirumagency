@extends('layouts.cms')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Category</div>

                    <div class="panel-body">
                       @include('admin.usermanagement._form',['route'=>['admin.usermanagement.update',$user->id],'user'=>$user,'method'=>'PUT'])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection