@extends('layouts.cms')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Article</div>

                    <div class="panel-body">
                      @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                       @include('article._form',['route'=>['article.update',$article->id],'article'=>$article,'category'=>$category,'method'=>'PUT'])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection