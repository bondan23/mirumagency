@extends('layouts.cms')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Your Article</div>

                    <div class="panel-body">
                       <a href="{{ url('article/create') }}" class="btn btn-primary" style="margin-bottom:10px;">
                           <i class="fa fa-plus"></i> Add New Article
                       </a>
                       @if($article->count() == 0)
                           <h2 class="text-center" style="margin-top:0px;">Belum Ada Artikel Tersedia</h2>
                       @else
                        <table class="table table-condensed table-striped table-bordered">
                            <tr class="text-center">
                                <td>No</td>
                                <td>Thumbnail</td>
                                <td>Title</td>
                                <td>Short Description</td>
                                <td>Category</td>
                                <td>Comments</td>
                                <td>Status</td>
                                <td>
                                    <i class="fa fa-gear"></i>
                                </td>
                            </tr>
                            <?php 
                                $no=1;
                            ?>
                            @foreach($article as $v)
                            <tr class="{{ ($v->status == true)?'success':'warning' }}">
                                <td class="text-center">
                                    {{ $no++ }}
                                </td>
                                <?php 
                                    $image = ($v->thumbnail == null)?$v->image:$v->thumbnail;
                                ?>
                                <td>
                                    <img class="img-responsive" src="{{ asset('uploaded_file').'/'.$image }}" alt="">
                                </td>
                                <td>
                                    {{ $v->title }}
                                </td>
                                <td>
                                    {{ $v->short_description }}
                                </td>
                                <td class="text-center">
                                    {{ $v->categories->categories }}
                                </td>
                                <td class="text-center">
                                        <a href="#" data-toggle="modal" data-target="#commentModal" onclick="get_comment('{{ $v->id }}')">
                                            <span class="badge">
                                                {{ $v->comments->count() }}
                                            </span>
                                        </a>
                                    </td>
                                <td class="text-center" width="13%">
                                    @if($v->status == false)
                                        Waiting Approval
                                    @else
                                        Published
                                    @endif
                                </td>
                                <td class="text-center" style="width:15%">
                                    <a target="_blank" href="{{ route('article.show',$v->id) }}" class="btn btn-info btn-xs">
                                        <i class="fa fa-search"></i> Preview
                                    </a>
                                    <a href="{{ route('article.edit',$v->id) }}" class="btn btn-primary btn-xs">
                                        <i class="fa fa-pencil"></i> Edit
                                    </a>
                                    <a href="javascript:void(0)" onclick="hapus_modal(this)"  data-id="{{ $v->id }}"  class="btn btn-danger btn-xs" data-hapus="article" data-redirect="article" data-route="article/{{$v->id}}" data-toggle="modal" data-target="#confirmModal">
                                        <i class="fa fa-trash"></i> Delete
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('modal')
    @include('modal_confirm',['modal_title'=>'Article'])
    @include('modal_comment',['redirect'=>'article'])
@endsection