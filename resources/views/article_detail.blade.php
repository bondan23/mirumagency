@extends('layouts.blog')


@section('content')
    <!-- Title -->
    <h1>{{$article->title}}</h1>

    <!-- Author -->
    <p class="lead">
        by <a href="#">{{ $article->users->fullname }}</a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p><span class="glyphicon glyphicon-time"></span> 
    Posted on {{ substr($article->created_at,0,12) }} at {{ substr($article->created_at,13,5)}}</p>

    <hr>

    <!-- Preview Image -->
    <img class="img-responsive" src="{{ asset('uploaded_file').'/'.$article->image }}" alt="">

    <hr>

    <!-- Post Content -->
    {!! $article->content !!}

    <hr>

    <!-- Blog Comments -->

    <!-- Comments Form -->
    @if(!Auth::guard('admin')->guest() || !Auth::guest() )
    <div class="well">
        <h4>Leave a Comment:</h4>
        <form role="form" action="{{ route('article.comment.store',$article->id) }}" method="POST">
           {{ csrf_field() }}
            <div class="form-group">
                <textarea name="comment" class="form-control" rows="3"></textarea>
            </div>
            @if ($errors->has('comment'))
                <span class="help-block">
                    <strong>{{ $errors->first('comment') }}</strong>
                </span>
            @endif
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    @else
    <div class="well">
        <h4 class="text-center"><a href="{{ url('login?redirect=').$article->id }}">Login</a> or <a href="{{ url('register?redirect=').$article->id }}">Register</a> to Leave a Comment</h4>
    </div>
    @endif

    <hr>

    <!-- Posted Comments -->

    <!-- Comment -->
    @if($article->comments->count() > 0)
        @foreach($article->comments as $comment)
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">{{ $comment->users->fullname }}
                        <small>{{ substr($comment->created_at,0,12) }} at {{ substr($comment->created_at,13,5)}}</small>
                    </h4>
                    {{ $comment->comment }}
                </div>
            </div>
        @endforeach
    @else
        <h5>Belum Ada Comment</h5>
    @endif     
@endsection