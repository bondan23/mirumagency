@extends('layouts.cms')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Category</div>

                    <div class="panel-body">
                       @include('category._form',['route'=>'category.store','category'=>$category,'method'=>'POST'])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection