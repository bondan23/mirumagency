@if($comments->count() > 0)
<table class="table table-striped">
    <tr>
        <td>No</td>
        <td>Comment</td>
        <td>User</td>
        <td class="text-center">
            <i class="fa fa-gear"></i>
        </td>
    </tr>
    <?php 
        $no=1;
    ?>
    @foreach($comments as $v)
    <tr>
        <td>
            {{ $no++ }}
        </td>
        <td>
            {{ $v->comment }}
        </td>
        <td>
            {{ $v->users->fullname }}
        </td>
        
        <td class="text-center">
            <a href="#" onclick="delete_comment('{{ $v->article_id }}','{{ $v->id }}')" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i> Delete </a>
        </td>
    </tr>
    @endforeach
</table>
@else
Belum Ada Komentar Di Artikel Ini.
@endif