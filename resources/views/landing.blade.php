@extends('layouts.blog')


@section('content')
<h1 class="page-header">
    Welcome to our blog.
</h1>

@foreach($article as $v)
<!-- First Blog Post -->
    <h2>
        <a href="{{ url('/view')."/".$v->slug }}">
            {{ $v->title }}
        </a>
    </h2>
    <p class="lead">
        by <a href="#">
            {{ $v->users->fullname }}
        </a>
    </p>

    <p><span class="glyphicon glyphicon-time"></span> Posted on {{ substr($v->created_at,0,12) }} at {{ substr($v->created_at,13,5)}}</p>
    <hr>
    <img class="img-responsive" src="{{ asset('uploaded_file').'/'.$v->image }}" alt="">
    <hr>
    <p>
        {{ $v->short_description }}
    </p>
    <a class="btn btn-primary" href="{{ url('/view')."/".$v->slug }}">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

    <hr>

@endforeach

<!-- Pager -->
{{ $article->appends(compact('q'))->links() }}

@endsection