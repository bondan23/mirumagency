<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mirumagency Test Backend">
    <meta name="author" content="Bondan Eko Prasetyo">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>@yield('title','MirumAgency')</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/blog-home.css') }}" rel="stylesheet">
    
    <script src="https://www.google.com/recaptcha/api.js"></script>
    
    <style>
        .scrollToTop{
            width:100px; 
            height:130px;
            padding:10px; 
            text-align:center; 
            background: whiteSmoke;
            font-weight: bold;
            color: #444;
            text-decoration: none;
            position:fixed;
            bottom:-40px;
            right:0px;
            display:none;
            background: url('arrow_up.png') no-repeat 0px 20px;
            z-index:100;
        }
        .scrollToTop:hover{
            text-decoration:none;
        }
        
        .affix{
            right:10px;
            top:53px;
        }
    </style>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">MirumAgency</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guard('admin')->guest() && Auth::guard()->guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                @if(Auth::guard('admin')->guest() == false)
                                    Hi,{{ Auth::guard('admin')->user()->fullname}} <span class="caret"></span>
                                @else
                                    Hi,{{ Auth::user()->fullname }} <span class="caret"></span>
                                @endif
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if(Auth::guard('admin')->guest() == false)
                                <li>
                                    <a href="{{ url('/admin') }}">Admin Dashboard</a>
                                </li>
                                <li>
                                        <a href="{{ url('/admin/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </li>
                                @else
                                <li>
                                    <a href="{{ url('/home') }}">User Dashboard</a>
                                </li>
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <a href="#" class="scrollToTop">
        <i class="fa fa-arrow-up" style="font-size:3em;"></i>
    </a>
    
    <!-- Page Content -->
    @if(Request::is('login') || Request::is('register') || Request::is('password/*'))
        @yield('content')
    @else
    <div class="container">

        <div class="row">
            
            <!-- Blog Content -->
            <div class="col-lg-8">
                @yield('content')
            </div>
            
            @if(!Request::is('login') && !Request::is('register'))
            <!-- Blog Sidebar Widgets Column -->
            @if($category->count() > 0)
                <div class="col-md-4" data-spy="affix" data-offset-top="20">
            @else
                <div class="col-md-4">  
            @endif
                 <!-- Blog Search Well -->
                 <div class="well">
                    <h4>Blog Search</h4>
                    {!! Form::open(['url'=>'/','method'=>'get']) !!}
                    <div class="input-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                    {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Search Article..']) !!}
                          {!! $errors->first('q', '<p class="help-block">:message</p>') !!}

                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </div>
                    {!! Form::close() !!}
                    <!-- /.input-group -->
                 </div>
                
                <div class="well">
                    <h4>Blog Categories</h4>
                    @if(isset($category))
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="accordion" class="panel-group">
                                @foreach($category as $v)

                                    @if($v->articlesCount > 0)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$v->id}}"> 
                                                {{ $v->categories }} <span class="badge pull-right">{{ $v->articlesCount }}</span>
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="collapse{{$v->id}}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ol>
                                                    @foreach($v->articles as $value)
                                                        @if($value->status == true)
                                                            <li>
                                                                <a href=" {{ route('blog.view',$value->slug) }}">
                                                                    {{ $value->title }}
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    <!-- /.col-lg-6 -->
                    </div>
                    @endif
                    <!-- /.row -->
                </div>
                
                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Ads Space Here</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>
                
                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Ads Space Here</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>

            </div>
            @endif
        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2016</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    @endif
    
    @yield('modal')

    <!-- jQuery -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    
    
    <script>
        var link = '<?php echo url('/') ?>/';
        $(document).ready(function(){
             //Check to see if the window is top if not then display button
            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });

            //Click event to scroll to top
            $('.scrollToTop').click(function(){
                $('html, body').animate({scrollTop : 0},800);
                return false;
            });
        })
    </script>

</body>

</html>
