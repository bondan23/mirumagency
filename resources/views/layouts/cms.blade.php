<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mirumagency Test Backend">
    <meta name="author" content="Bondan Eko Prasetyo">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>@yield('title','MirumAgency')</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/blog-home.css') }}" rel="stylesheet">
    
    <style>
        .scrollToTop{
            width:100px; 
            height:130px;
            padding:10px; 
            text-align:center; 
            background: whiteSmoke;
            font-weight: bold;
            color: #444;
            text-decoration: none;
            position:fixed;
            bottom:10px;
            right:0px;
            display:none;
            background: url('arrow_up.png') no-repeat 0px 20px;
            z-index:100;
        }
        .scrollToTop:hover{
            text-decoration:none;
        }
    </style>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">MirumAgency</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @if(Auth::user()->is_admin == true)
                        <li>
                            <a href="{{ url('/admin') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.article.index' )}}">
                                Articles 
                                <?php 
                                    $article = \App\Article::where('status',false)->get();
                                    $count = $article->count();
                                ?>
                                @if($count != 0)
                                <span class="badge">
                                    {{ $count }}
                                </span>
                                @endif
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.category.index')}}">Categories</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.usermanagement.index') }}">User Management</a>
                        </li>
                    @else
                        <li>
                            <a href="{{ url('/home') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('article.index') }}">Articles</a>
                        </li>
                        <li>
                            <a href="{{ route('category.index')}}">Categories</a>
                        </li>
                    @endif
                </ul>
                
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guard('admin')->guest() && Auth::guard()->guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               Hi,{{ Auth::user()->fullname }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/') }}">View Site</a>
                                </li>
                                @if(Auth::user()->is_admin == true)
                                <li>
                                    <a href="{{ route('admin.changepass') }}">
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                @else
                                <li>
                                    <a href="{{ route('user.changepass') }}">
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <a href="#" class="scrollToTop">
        <i class="fa fa-arrow-up" style="font-size:3em;"></i>
    </a>
    
    @if (session()->has('flash_notification.message'))
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="alert alert-success text-center">
                    {{ session()->get('flash_notification.message') }}
                </div>
            </div>
        </div>
    </div>
    @endif
    
    <!-- Page Content -->
    @yield('content')
    
    @yield('modal')

    <!-- jQuery -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
    
    
    <script>
        var link = '<?php echo url('/') ?>/';
        CKEDITOR.replace( 'editor' );
    </script>
    
    <script src="{{ asset('js/core.js') }}"></script>
    

</body>

</html>
