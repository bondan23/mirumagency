<div class="modal modal-primary fade" tabindex="-1" role="dialog" id="commentModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">View Comments</h4>
      </div>
      <div class="modal-body content_comments" data-redirect="{{ $redirect }}">
            Loading...
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->