<div class="modal modal-primary fade" tabindex="-1" role="dialog" id="confirmModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Konfirmasi Hapus {{ $modal_title }}</h4>
      </div>
      <div class="modal-body">
        <p>Yakin ingin menghapus {{ strtolower($modal_title) }} ini ? </p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success hapus_{{ strtolower($modal_title) }}" type="button" onclick="hapus(this)">Iya</button>
        <button class="btn btn-danger" type="button" data-dismiss="modal" >Tidak</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->