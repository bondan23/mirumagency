<?php

Route::group(['prefix' => 'admin','namespace'=>'Admin'], function() {
    Route::get('/login', ['as' => 'admin.getLogin', 'uses' => 'AuthController@showLoginForm'])->middleware(['guest:admin','guest']);
    //kalau user sudah login,lalu mau mengarah ke login admin akan di lempar balik ke home...
    Route::post('/login', ['as' => 'admin.postLogin', 'uses' => 'AuthController@login']);
    Route::post('/logout', ['as' => 'admin.getLogout', 'uses' => 'AuthController@logout']);

    Route::group(['middleware' => ['auth.admin:admin']], function() {
        Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminController@index']);
    });
    
    /*ARTICLE CMS*/
    Route::group(['prefix' => 'article'],function(){
        Route::get('/view','ArticleController@index')->name('admin.article.index');
        Route::get('/create','ArticleController@create')->name('admin.article.create');
        Route::post('/create','ArticleController@store')->name('admin.article.store');
        Route::get('/{id}/edit','ArticleController@edit')->name('admin.article.edit');
        Route::put('/{id}/edit','ArticleController@update')->name('admin.article.update');
        Route::delete('/delete/{id}','ArticleController@destroy')->name('admin.article.destroy');
        Route::post('/approve','ArticleController@bulkAction')->name('admin.article.approve');
    });
    
    /*CATEGORY CMS*/
    Route::group(['prefix' => 'category'],function(){
        Route::get('/view','CategoryController@index')->name('admin.category.index');
        Route::get('/preview/{id}','CategoryController@show')->name('admin.category.show')->where('id','[0-9]+');
        Route::get('/create','CategoryController@create')->name('admin.category.create');
        Route::post('/create','CategoryController@store')->name('admin.category.store');
        Route::get('/{id}/edit','CategoryController@edit')->name('admin.category.edit');
        Route::put('/{id}/edit','CategoryController@update')->name('admin.category.update');
        Route::delete('/delete/{id}','CategoryController@destroy')->name('admin.category.destroy');
    });
    
    /*USER MANAGEMENT CMS*/
    Route::group(['prefix' => 'usermanagement'],function(){
        Route::get('/view','UserManagementController@index')->name('admin.usermanagement.index');
        Route::get('/preview/{id}','UserManagementController@show')->name('admin.usermanagement.show')->where('id','[0-9]+');
        Route::get('/create','UserManagementController@create')->name('admin.usermanagement.create');
        Route::post('/create','UserManagementController@store')->name('admin.usermanagement.store');
        Route::get('/{id/edit}','UserManagementController@edit')->name('admin.usermanagement.edit');
        Route::put('/{id/edit}','UserManagementController@update')->name('admin.usermanagement.update');
        Route::delete('/delete/{id}','UserManagementController@destroy')->name('admin.usermanagement.destroy');
    });
    
    Route::get('/changepassword','AdminController@changePasswordView')->name('admin.changepass');
    Route::put('/changepassword/{id}','AdminController@changePasswordStore')->name('admin.storepass')->where('id','[0-9]+');
});