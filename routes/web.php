<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','HomeController@landingPage');

Auth::routes();

Route::get('/home', 'UserController@index');

Route::get('/view/{slug?}','HomeController@detail')->name('blog.view');

/*Route::group(['prefix' => 'article'],function(){
    Route::get('/view','ArticleController@index')->name('article.index');
    Route::get('/preview/{id}','ArticleController@show')->name('article.show')->where('id','[0-9]+');
    Route::get('/create','ArticleController@create')->name('article.create');
    Route::post('/create','ArticleController@store')->name('article.store');
    Route::get('/edit/{id}','ArticleController@edit')->name('article.edit');
    Route::put('/edit/{id}','ArticleController@update')->name('article.update');
    Route::delete('/delete/{id}','ArticleController@destroy')->name('article.destroy');
});*/

/*Route::group(['prefix' => 'category'],function(){
    Route::get('/view','CategoryController@index')->name('category.index');
    Route::get('/preview/{id}','CategoryController@show')->name('category.show')->where('id','[0-9]+');
    Route::get('/create','CategoryController@create')->name('category.create');
    Route::post('/create','CategoryController@store')->name('category.store');
    Route::get('/edit/{id}','CategoryController@edit')->name('category.edit');
    Route::put('/edit/{id}','CategoryController@update')->name('category.update');
    Route::delete('/delete/{id}','CategoryController@destroy')->name('category.destroy');
});*/

Route::resource('article','ArticleController');
Route::resource('category','CategoryController');
Route::resource('article.comment', 'CommentController');

Route::get('/socialauth/{provider}','SocialController@redirect');
Route::get('/socialauth/{provider}/callback','SocialController@callback');

Route::get('/changepassword','UserController@changePasswordView')->name('user.changepass');
Route::put('/changepassword/{id}','UserController@changePasswordStore')->name('user.storepass')->where('id','[0-9]+');